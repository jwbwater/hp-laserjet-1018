# HP Laserjet 1018
This printer has firmware that has to be provided each time it is turned on. It
is connected to a router running Tomato Firmware which loads this firmware.

## Fedora
Install:
        foo2zjs

Use the CUPS web UI at http://localhost:631/.

	network printer protocol: AppSocket/HP JetDirect 

	connection: socket://192.168.1.1:9100

	make: HP

	model: HP Laserjet 1018 Foomatic/foo2zjs-z1 (recommended) (en)

## Chrome OS
On Chrome OS adding this printer has proven more challenging. The following
packages were installed:

	foomatic-db-engine
	foomatic-db-compressed-ppds
	printer-driver-foo2zjs
	printer-driver-foo2zjs-common

Since CUPS is running inside a virtual machine on Chrome OS, to access the CUPS
web UI, get the IP address inside the terminal using 'ip addr' and use that in
the URL as in this example where ip addr returned 100.115.92.197:

	https://100.115.92.197:631/

Then add the printer as in Debian.

Print a test page from the CUPS web interface to confirm it works.

To print from Chrome OS, I had to add this Chrome extension:

	IPP / CUPS printing for Chrome & Chromebooks


These commands were also run in an effort to share this printer with the VM
host running Chrome OS.

	xhost +

	sudo cupsctl --remote-admin --remote-any --share-printers

	sudo systemctl restart cups


